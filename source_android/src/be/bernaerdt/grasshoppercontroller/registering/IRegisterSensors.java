package be.bernaerdt.grasshoppercontroller.registering;

public interface IRegisterSensors
{
	public void Register(boolean touch, boolean light, boolean accelero, boolean orientation, int speed);
}
