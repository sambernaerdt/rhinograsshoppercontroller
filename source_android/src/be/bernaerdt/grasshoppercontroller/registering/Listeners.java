package be.bernaerdt.grasshoppercontroller.registering;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import be.bernaerdt.grasshoppercontroller.ISendActivity;
import be.bernaerdt.grasshoppercontroller.sending.SendInfo;

public class Listeners implements SensorEventListener, OnTouchListener
{

	private SendInfo _sendInfo;
	private ISendActivity _sendActivity;

	public Listeners(SendInfo sendInfo, ISendActivity sendActivity)
	{
		_sendInfo = sendInfo;
		_sendActivity = sendActivity;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy)
	{
		// Don't do anything
	}

	public void onSensorChanged(SensorEvent event)
	{
		switch (event.sensor.getType())
		{
		case Sensor.TYPE_ACCELEROMETER:
		{
			// Only update if difference is >0.001
			float s0 = event.values[0] / SensorManager.GRAVITY_EARTH;
			float s1 = event.values[1] / SensorManager.GRAVITY_EARTH;
			float s2 = event.values[2] / SensorManager.GRAVITY_EARTH;

			if (Math.abs(s0 - _sendInfo.AcceleroValues[0]) > 0.001
					|| Math.abs(s1 - _sendInfo.AcceleroValues[1]) > 0.001
					|| Math.abs(s2 - _sendInfo.AcceleroValues[2]) > 0.001)
			{
				_sendInfo.AcceleroValues[0] = s0;
				_sendInfo.AcceleroValues[1] = s1;
				_sendInfo.AcceleroValues[2] = s2;
				_sendActivity.SensorChanged();
			}
			break;
		}
		case Sensor.TYPE_LIGHT:
		{
			_sendInfo.LightValues = event.values[0];
			_sendActivity.SensorChanged();
			break;
		}
		case Sensor.TYPE_MAGNETIC_FIELD:
		{
			float[] rotationMatrix = new float[9];
			SensorManager.getRotationMatrix(rotationMatrix, null, _sendInfo.AcceleroValues, event.values);

			float[] orientation = new float[3];
			SensorManager.getOrientation(rotationMatrix, orientation);

			// Only update if difference is >0.001
			float s0 = event.values[0];
			float s1 = event.values[1];
			float s2 = event.values[2];

			if (Math.abs(s0 - _sendInfo.OrientationValues[0]) > 0.001
					|| Math.abs(s1 - _sendInfo.OrientationValues[1]) > 0.001
					|| Math.abs(s2 - _sendInfo.OrientationValues[2]) > 0.001)
			{
				_sendInfo.OrientationValues = orientation.clone();
				_sendActivity.SensorChanged();
			}
			break;
		}
		}
	}

	public boolean onTouch(View v, MotionEvent event)
	{
		try
		{
			int action = event.getAction() & MotionEvent.ACTION_MASK;
			int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
			int pointerId = event.getPointerId(pointerIndex);

			switch (action)
			{
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				_sendInfo.TouchTouchedValues[pointerId] = true;
				_sendInfo.TouchXValues[pointerId] = (int) event.getX(pointerIndex);
				_sendInfo.TouchYValues[pointerId] = (int) event.getY(pointerIndex);
				break;

			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
				_sendInfo.TouchTouchedValues[pointerId] = false;
				_sendInfo.TouchXValues[pointerId] = (int) event.getX(pointerIndex);
				_sendInfo.TouchYValues[pointerId] = (int) event.getY(pointerIndex);
				break;

			case MotionEvent.ACTION_MOVE:
				int pointerCount = event.getPointerCount();
				for (int i = 0; i < pointerCount; i++)
				{
					if (i < 10)
					{
						pointerIndex = i;
						pointerId = event.getPointerId(pointerIndex);
						_sendInfo.TouchXValues[pointerId] = (int) event.getX(pointerIndex);
						_sendInfo.TouchYValues[pointerId] = (int) event.getY(pointerIndex);
					}
				}
				break;
			}
		}
		catch (Exception ex)
		{
			// Do nothing...
		}

		_sendActivity.SensorChanged();

		return true;
	}
}
