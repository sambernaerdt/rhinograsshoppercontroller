package be.bernaerdt.grasshoppercontroller.registering;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.view.View;
import be.bernaerdt.grasshoppercontroller.sending.SendInfo;

public class RegisterSensors implements IRegisterSensors
{
	SendInfo _sendInfo;
	Listeners _listeners;
	View _toRegister;
	int _speed;
	SensorManager _sensorManager;
	
	public RegisterSensors(View toRegister, SendInfo sendInfo, Listeners listeners, SensorManager sensorManager)
	{
		_sendInfo = sendInfo;
		_listeners = listeners;
		_toRegister = toRegister;
		_sensorManager = sensorManager;
	}

	public void Register(boolean touch, boolean light, boolean accelero, boolean orientation, int speed)
	{
		_speed = speed;
		_registerTouch(touch);
		_registerLightSensors(light);
		_registerAcceleroMeter(accelero);
		// Orientation should come after accelero
		_registerOrientation(orientation);
	}
	
	private void _registerTouch(boolean register)
	{
		if (!register)
		{
			_sendInfo.TouchOn = false;
			_sendInfo.TouchInfo = "Turned off";
		}
		else
		{
			_sendInfo.TouchOn = true;
			_sendInfo.TouchInfo = "";
			_toRegister.setOnTouchListener(_listeners);
		}
	}

	private void _registerLightSensors(boolean register)
	{
		_sendInfo.LightOn = false;
		
		if (!register)
			_sendInfo.LightInfo = "Turned off";
		else if (_sensorManager.getSensorList(Sensor.TYPE_LIGHT).size() == 0)
			_sendInfo.LightInfo = "Couldn't find light Sensor.";
		else
		{
			Sensor lightSensor = _sensorManager.getSensorList(Sensor.TYPE_LIGHT).get(0);
			if (!_sensorManager.registerListener(_listeners, lightSensor, _speed))
				_sendInfo.LightInfo = "Couldn't register light sensor.";
			else
			{
				_sendInfo.LightOn = true;
				_sendInfo.LightInfo = "";
			}
		}
	}

	private void _registerAcceleroMeter(boolean register)
	{
		_sendInfo.AcceleroOn = false;

		if (!register)
			_sendInfo.AcceleroInfo = "Turned off";
		else if (_sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() == 0)
			_sendInfo.AcceleroInfo = "Couldn't find Accelerometer.";
		else
		{
			Sensor accelerometer = _sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);

			if (!_sensorManager.registerListener(_listeners, accelerometer, _speed))
				_sendInfo.AcceleroInfo = "Couldn't register accelerometer";
			else
			{
				_sendInfo.AcceleroOn = true;
				_sendInfo.AcceleroInfo = "";
			}
		}
	}
	
	/*
	 * Registers AcceleroMeter for orientation
	 */
	private void registerAccelerometerSilent()
	{
		//if it was already turned on by user, just skip
		if (_sendInfo.AcceleroOn)
		{
			_sendInfo.AcceleroSilentOn = true;
			return;
		}
		
		_sendInfo.AcceleroSilentOn = false;
		if (_sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0)
		{
			Sensor accelerometer = _sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
			if (_sensorManager.registerListener(_listeners, accelerometer, _speed))
				_sendInfo.AcceleroSilentOn = true;	
		}
	}
	
	private void _registerOrientation(boolean register)
	{
		_sendInfo.OrientationOn = false;
		
		if (!register)
			_sendInfo.OrientationInfo = "Turned off";
		else if (_sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).size() == 0)
			_sendInfo.OrientationInfo = "Couldn't find Magnetic Field Sensor (required for rotation).";
		else
		{
			// Rotation needs accelero AND magnetic. If accelero is not turnedon, turn it on now 
			if (!_sendInfo.AcceleroOn)
				registerAccelerometerSilent();
			
			// if failed, set rotation also to failed
			if (!_sendInfo.AcceleroOn && !_sendInfo.AcceleroSilentOn)
				_sendInfo.OrientationInfo = "Couldn't register Accelerometer (required for rotation).";
			else
			{
				Sensor magneticMeter = _sensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD).get(0);
				if (!_sensorManager.registerListener(_listeners, magneticMeter, _speed))
					_sendInfo.OrientationInfo = "Couldn't register Magnetic Field Sensor (required for rotation).";
				else
				{
					_sendInfo.OrientationOn = true;
					_sendInfo.OrientationInfo = "";
				}
			}
		}
	}


}
