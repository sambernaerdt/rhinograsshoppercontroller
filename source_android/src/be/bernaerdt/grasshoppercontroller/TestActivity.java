package be.bernaerdt.grasshoppercontroller;

import java.util.ArrayList;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import be.bernaerdt.grasshoppercontroller.gui.SettingsItemAdapter;
import be.bernaerdt.grasshoppercontroller.gui.SettingsVM;

public class TestActivity extends Activity
{

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState); 
	    
	    setContentView(R.layout.test);
	    
	    ArrayList<SettingsVM> list = new ArrayList<SettingsVM>();
	    SettingsVM a = new SettingsVM();
	    a.Title = "First";
	    a.Details = "Some 1 Detials";
	    list.add(a);
	    SettingsVM b = new SettingsVM();
	    b.Title = "Second";
	    b.Details = "Some 2 Detials";
	    list.add(b);
	    SettingsVM c = new SettingsVM();
	    c.Title = "Third";
	    c.Details = "Some 3 Detials";
	    list.add(c);
	    
	    
        final ListView lv1 = (ListView) findViewById(R.id.listView_settingsOverview);
        SettingsItemAdapter adapt =new SettingsItemAdapter(this, list); 
        lv1.setAdapter(adapt);
        
        
        OnItemClickListener click_listener = new OnItemClickListener()
        {
        	public void onItemClick(AdapterView<?> a, View v, int position, long id)
			{
/*
 * Object o = lv1.getItemAtPosition(position);
            	ItemDetails obj_itemDetails = (ItemDetails)o;
        		Toast.makeText(ListViewImagesActivity.this, "You have chosen : " + " " + obj_itemDetails.getName(), Toast.LENGTH_LONG).show();				
 */
        		// Toast.makeText(TestActivity.this, "Something was clicked!", Toast.LENGTH_LONG).show();
        		Intent i = new Intent(TestActivity.this, ShowSettingsSensorActivity.class);
        		startActivity(i);
			}  
        };
        
        
        lv1.setOnItemClickListener(click_listener);
        
	}
}
