package be.bernaerdt.grasshoppercontroller;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import be.bernaerdt.grasshoppercontroller.gui.MainGUI;
import be.bernaerdt.grasshoppercontroller.wifi.WifiSettings;

public class InitTask extends AsyncTask<Context, String, Boolean>
{
	private String _lastError = "";
	private MainGUI _mainGUI;
	private WifiSettings _wifiSettings;

	public InitTask(MainActivity mainActivity)
	{
		_mainGUI = mainActivity.mainGUI;
		_wifiSettings = mainActivity.wifiSettings;
	}

	public void Report(String... values)
	{
		publishProgress(values);
	}
	
	@Override
	protected Boolean doInBackground(Context... params)
	{
		try
		{
			if (!_wifiSettings.tryToEnableWifi(this))
				return false;

			if (!_wifiSettings.tryToConnectWifi(this))
				return false;

			if (!_wifiSettings.tryToGetBroadcastAddress(this))
				return false;

			if (!_wifiSettings.tryToFindAvailablePort(this))
				return false;

			return true;
		}
		catch (Exception ex)
		{
			_lastError = "Error: " + ex.getMessage();
			return false;
		}
	}

	@Override
	protected void onPreExecute()
	{
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(String... values)
	{
		super.onProgressUpdate(values);
		_mainGUI.reportProgress(values[0], values[1]);
	}

	@Override
	protected void onCancelled()
	{
		super.onCancelled();
		_mainGUI.reportProgress("Cancelled!", null);
	}

	@Override
	protected void onPostExecute(Boolean result)
	{
		super.onPostExecute(result);

		if (result)
			startUpFinished();
		else
			_mainGUI.reportProgress("Startup failed: " + _lastError, null);

	}

	private void startUpFinished()
	{
		// If we're here, hide the log and show the details
		_mainGUI.HideStartUpLog();
		_mainGUI.SetConfigurationText(
				String.valueOf(_wifiSettings.port),
				_wifiSettings.inetAddress.toString(),
				_wifiSettings.wifiName);
	}

}
