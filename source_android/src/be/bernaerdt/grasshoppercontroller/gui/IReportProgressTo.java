package be.bernaerdt.grasshoppercontroller.gui;

public interface IReportProgressTo
{
	void reportProgress(String logText, String curText);
}
