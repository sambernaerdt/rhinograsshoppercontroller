package be.bernaerdt.grasshoppercontroller.gui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import be.bernaerdt.grasshoppercontroller.R;
import be.bernaerdt.grasshoppercontroller.R.id;
import be.bernaerdt.grasshoppercontroller.sending.SendInfo;

public class SendGUI
{

	// GUI
	public TextView _acceleroValues,
					_rotationValues,
					_lightValue,
					_touchValues,
					_sendStatusLastTry,
					_startupView;
	public TableLayout _succesView;
	public LinearLayout _sendView;
	
	private Activity _context;

	private Calendar _calendar;
	private DateFormat _formatter = new SimpleDateFormat("HH:mm:ss.SSS");
	
	public View getRoot()
	{
		return _sendView;
	}
	
	public SendGUI(Activity context)
	{
		_context = context;
		_calendar = Calendar.getInstance();
		
		_acceleroValues = (TextView) find(R.id.AcceleroMeterValue);
		_rotationValues = (TextView) find(R.id.RotationValue);
		_lightValue = (TextView) find(R.id.LightMeterValue);
		_touchValues = (TextView) find(R.id.TouchValue);
		_sendStatusLastTry = (TextView) find(R.id.lastUpdateValue);
		_startupView = (TextView) find(R.id.loadFailedView);
		_succesView = (TableLayout) find(R.id.loadSuccesView);
		_sendView = (LinearLayout) find(R.id.MainSendLayout);
	}

	private Object find(int id)
	{
		return _context.findViewById(id);
	}

	public void setError(String string)
	{
		// GUI : Switch views
		_startupView.setVisibility(View.VISIBLE);
		_succesView.setVisibility(View.GONE);

		_startupView.setText(string);
	}

	public void hideError()
	{
		_startupView.setVisibility(View.GONE);
		_succesView.setVisibility(View.VISIBLE);
	}
	
	public void updateGUIFields(SendInfo sendInfo)
	{
		if(sendInfo==null)
			return;
		
		if (_acceleroValues != null)
			_acceleroValues.setText(sendInfo.acceleroString());
		if (_rotationValues != null)
			_rotationValues.setText(sendInfo.rotationString());
		if (_lightValue != null)
			_lightValue.setText(sendInfo.lightString());
		if (_touchValues != null)
			_touchValues.setText(sendInfo.touchString());
	}

	public void updateSucces()
	{
		if (_sendStatusLastTry != null)
		{
			_calendar.setTimeInMillis(System.currentTimeMillis());
			_sendStatusLastTry.setText(_formatter.format(_calendar.getTime()));
		}
	}

	public void updateFail()
	{
		if (_sendStatusLastTry != null)
			_sendStatusLastTry.setText("Failed");
	}
	
	
}
