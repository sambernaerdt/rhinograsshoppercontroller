package be.bernaerdt.grasshoppercontroller.gui;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import be.bernaerdt.grasshoppercontroller.R;

public class SettingsItemAdapter extends BaseAdapter
{
	private static ArrayList<SettingsVM> itemDetailsrrayList;
	private LayoutInflater l_Inflater;

	public SettingsItemAdapter(Context context, ArrayList<SettingsVM> list)
	{
		itemDetailsrrayList = list;
		l_Inflater = LayoutInflater.from(context);
	}

	public int getCount()
	{
		return itemDetailsrrayList.size();
	}

	public Object getItem(int position)
	{
		return itemDetailsrrayList.get(position);
	}

	public long getItemId(int position)
	{
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder;
		if (convertView == null)
		{
			convertView = l_Inflater.inflate(R.layout.listitemsettingsoverview, null);
			holder = new ViewHolder();
			holder.itemTitle = (TextView) convertView.findViewById(R.id.ListItemSettingsOverView_Title);
			holder.itemDescription = (TextView) convertView.findViewById(R.id.ListItemSettingsOverView_Description);
			//holder.itemIcon = (ImageView) convertView.findViewById(R.id.ListItemSettingsOverView_Icon);

			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.itemTitle.setText(itemDetailsrrayList.get(position).Title);
		holder.itemDescription.setText(itemDetailsrrayList.get(position).Details);
		//holder.itemIcon.setImageResource(imgid[itemDetailsrrayList.get(position).getImageNumber() - 1]);

		return convertView;
	}

	static class ViewHolder
	{
		TextView itemTitle;
		TextView itemDescription;
		ImageView itemIcon;
	}
}
