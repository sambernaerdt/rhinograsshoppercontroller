package be.bernaerdt.grasshoppercontroller.gui;

import be.bernaerdt.grasshoppercontroller.MainActivity;
import be.bernaerdt.grasshoppercontroller.R;
import be.bernaerdt.grasshoppercontroller.Settings.MainSettings;
import be.bernaerdt.grasshoppercontroller.Settings.SettingBase;
import android.app.Activity;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MainGUI implements IReportProgressTo
{
	private Activity _activity;
	
	private String _log = "";
	
	// Controls
	private TextView _logView;
	private LinearLayout _startUpPanel;
	private LinearLayout _succesPanel;
	private Spinner _spinnerOrientation;
	private Spinner _spinnerAccelero;
	private Spinner _spinnerLight;
	private Spinner _spinnerTouch;
	private Spinner _spinnerSpeed;
	private Spinner _spinnerSendSpeed;
	private TextView _curStepText;
	private LinearLayout _curStepPanel;
	private TextView _portText;
	private TextView _inetAddressText;
	private TextView _networkText;

	// OCP & SRP Violation -> Create MAP (see SettingInt) for spinner, setting (and more)
	public MainGUI(MainActivity mainActivity)
	{
		_activity = mainActivity;
		
		_logView = (TextView) _activity.findViewById(R.id.LogInfo);
		_succesPanel = (LinearLayout) _activity.findViewById(R.id.SuccesInfoPanel);
		_startUpPanel = (LinearLayout) _activity.findViewById(R.id.startUpInfoPanel);

		MainSettings settings = mainActivity.mainSettings;
		
		_initializeSpinner(_spinnerAccelero, R.id.spinnerAcceleroMeter, R.array.on_off, settings.accelerometerOn);
		_initializeSpinner(_spinnerLight, R.id.spinnerLightMeter, R.array.on_off, settings.lightOn);
		_initializeSpinner(_spinnerOrientation, R.id.spinnerRotationMeter, R.array.on_off, settings.orientationOn);
		_initializeSpinner(_spinnerTouch, R.id.spinnerTouch, R.array.on_off, settings.touchOn);
		// TODO: streamline naming of speeds
		_initializeSpinner(_spinnerSpeed, R.id.spinnerSpeed, R.array.update_speeds, settings.sensorSpeed);
		_initializeSpinner(_spinnerSendSpeed, R.id.spinnerSendSpeed, R.array.send_speeds, settings.sendSpeed);
		
		
		_curStepText = (TextView) _activity.findViewById(R.id.curStepInfo);
		_curStepPanel = (LinearLayout) _activity.findViewById(R.id.curStepPanel);
		_portText = (TextView) _activity.findViewById(R.id.PortValue);
		_inetAddressText = (TextView) _activity.findViewById(R.id.AddressValue);
		_networkText = (TextView) _activity.findViewById(R.id.NetworkValue);
	}


	// TODO: convert to ISetting
	private void _initializeSpinner(Spinner spinner, int rId, int rArray, SettingBase setting)
	{
		spinner = (Spinner) _activity.findViewById(rId);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(_activity, rArray, R.layout.spinnerlayout);
		adapter.setDropDownViewResource(R.layout.spinneritemlayout);
		spinner.setAdapter(adapter);
		spinner.setSelection(setting.getSpinnerPosition());
	}
	
	public void HideStartUpLog()
	{
		_startUpPanel.setVisibility(View.GONE);
		_succesPanel.setVisibility(View.VISIBLE);
	}

	public void SetConfigurationText(String port, String inetAddress, String networkName)
	{
		
		if (_portText != null)
			_portText.setText(port);
		
		if (_inetAddressText != null)
			_inetAddressText.setText(inetAddress);

		if (_networkText != null)
			_networkText.setText(networkName);
	}
	
	/*
	 * Displays the log (if logtext is not null, log is appended)
	 * And the current step (if cuText is not null)
	 */
	private void setStartUpText(String logText, String curText)
	{
		if (logText != null)
		{
			if (_log == "")
				_logView.setVisibility(View.VISIBLE);
			else
				_log += "\n";
			_log += logText;
			if (_logView != null)
				_logView.setText(_log);
		}

		if (curText != null)
		{
			if (_curStepText != null)
				_curStepText.setText(curText);
			if (_curStepPanel != null)
				_curStepPanel.setVisibility(View.VISIBLE);
		}
		else
		{
			if (_curStepPanel != null)
				_curStepPanel.setVisibility(View.GONE);
		}
	}

	// OCP & SRP Violation
	public void updateSettings(MainSettings mainSettings) throws Exception
	{
		mainSettings.accelerometerOn.setFromSpinnerPosition(_spinnerAccelero.getSelectedItemPosition());
		mainSettings.lightOn.setFromSpinnerPosition(_spinnerLight.getSelectedItemPosition());
		mainSettings.orientationOn.setFromSpinnerPosition(_spinnerOrientation.getSelectedItemPosition());
		mainSettings.touchOn.setFromSpinnerPosition(_spinnerTouch.getSelectedItemPosition());
		mainSettings.sensorSpeed.setFromSpinnerPosition(_spinnerSpeed.getSelectedItemPosition());
		mainSettings.sendSpeed.setFromSpinnerPosition(_spinnerSendSpeed.getSelectedItemPosition());
	}

	public void reportProgress(String logText, String curText)
	{
		setStartUpText(logText, curText);
	}
}
