package be.bernaerdt.grasshoppercontroller.sending;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.util.Log;

public class SendOutIP implements ISendOut
{
	static final String TAG = SendOutIP.class.getName();
	
	InetAddress _inetAddress;
	int _port;
	
	public SendOutIP(InetAddress address, int port)
	{
		_inetAddress = address;
		_port = port;
		Log.i(TAG, "Using: " + address.getHostAddress());
		
		try
		{
			if(_inetAddress.isReachable(5000))
				Log.i(TAG, "Able to reach host");
			else
				Log.i(TAG, "Unable to reach host");
		}
		catch (IOException e)
		{
			Log.e(TAG, "Unable to reach host: " + e.getMessage());
		}
	}
	
	public void SendPackage(String infoToSend) throws Exception
	{
		DatagramSocket socket;
		DatagramPacket packet;
		
		try
		{
			socket = new DatagramSocket();
			socket.setBroadcast(false);
			socket.setSoTimeout(5000);
	
			packet = new DatagramPacket(infoToSend.getBytes(), infoToSend.length(), _inetAddress, _port);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Unable to create packet to send.");
		}
		
		try
		{
			socket.send(packet);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Unable to send the packet.");
		}
	}

}
