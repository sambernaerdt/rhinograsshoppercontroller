package be.bernaerdt.grasshoppercontroller.sending;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class SendOutBroadcast implements ISendOut
{
	// network
	private InetAddress _inetAddress;
	private int _port;
	
	public SendOutBroadcast(InetAddress address, int port)
	{
		// Network info
		_inetAddress = address;
		_port = port;
	}
	
	public void SendPackage(String infoToSend) throws Exception
	{
		DatagramSocket socket;
		DatagramPacket packet;
		
		try
		{
			socket = new DatagramSocket();
			socket.setBroadcast(true);
			socket.setSoTimeout(3000);
	
			packet = new DatagramPacket(infoToSend.getBytes(), infoToSend.length(), _inetAddress, _port);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Unable to create packet to send.");
		}
		
		try
		{
			socket.send(packet);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new Exception("Unable to send the packet.");
		}

	}

}
