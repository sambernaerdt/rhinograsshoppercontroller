package be.bernaerdt.grasshoppercontroller.sending;

public interface ISendInfo
{
	public String compose(String newLineChar);
}
