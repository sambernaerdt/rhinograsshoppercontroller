package be.bernaerdt.grasshoppercontroller.sending;

import java.text.DecimalFormat;

import android.content.Context;
import android.util.DisplayMetrics;

public class SendInfo implements ISendInfo
{
	// Should be multi thread safe
	
	// Device info
	public float _xDim;
	public float _yDim;

	// Parts to send
	public float[] AcceleroValues = new float[3];
	public float[] MagneticValues = new float[3];
	public float[] OrientationValues = new float[3];
	public float LightValues = 0;
	public float[] TouchXValues = new float[10];
	public float[] TouchYValues = new float[10];
	public boolean[] TouchTouchedValues = new boolean[10];

	public String[] AdditionalInfo = new String[10];

	// Registration status
	public String AcceleroInfo;
	public String OrientationInfo;
	public String LightInfo;
	public String TouchInfo;
	public boolean OrientationOn;
	public boolean LightOn;
	public boolean AcceleroOn;
	public boolean AcceleroSilentOn;
	public boolean TouchOn;

	private StringBuilder _s;
	private StringBuilder _sComp;
	
	public SendInfo(int x, int y)
	{
		_xDim = x;
		_yDim = y;

		 _s = new StringBuilder();
		 // TODO DEBUG: remove the following line to force failed send:
		 _sComp = new StringBuilder();
	}

	/*
	 * String for sending
	 */
	public synchronized String compose(String newLineChar)
	{
		_sComp.setLength(0);

		for (float a : AcceleroValues)
		{
			if(AcceleroOn)
				_sComp.append(a + newLineChar);
			else
				_sComp.append("0.0" + newLineChar);
		}

		for (float a : OrientationValues)
		{
			_sComp.append(a + newLineChar);
		}

		_sComp.append(LightValues + newLineChar);

		for (int i = 0; i < 10; i++)
		{
			_sComp.append(TouchTouchedValues[i] + newLineChar);
			_sComp.append(TouchXValues[i] / _xDim + newLineChar);
			_sComp.append(TouchYValues[i] / _yDim + newLineChar);
		}

		if (AdditionalInfo != null)
		{
			for (String a : AdditionalInfo)
			{
				_sComp.append(a + newLineChar);
			}
		}

		return _sComp.toString();
	}

	/*
	 * Creates string for Gui
	 */
	public String rotationString()
	{
		if (!OrientationOn)
			return OrientationInfo;
		else
		{
			_s.setLength(0);
			
			try
			{

				if (OrientationValues == null)
				{
					_s.append("(no values)");
					return _s.toString();
				}

				if (OrientationInfo != null && OrientationInfo != "")
					_s.append(OrientationInfo + "\n");

				for (int i = 0; i < OrientationValues.length; i++)
				{

					DecimalFormat df = new DecimalFormat("#.###");

					_s.append(df.format(OrientationValues[i]));

					if (i < OrientationValues.length - 1)
						_s.append("\n");
				}

			}
			catch (Exception ex)
			{
				_s.append("error while displaying values");
			}

			return _s.toString();
		}
	}

	/*
	 * Creates string for Gui
	 */
	public String acceleroString()
	{

		if (!AcceleroOn)
			return AcceleroInfo;
		else
		{
			_s.setLength(0);

			try
			{

				if (AcceleroValues == null)
				{
					_s.append("(no values)");
					return _s.toString();
				}

				if (AcceleroInfo != null && AcceleroInfo != "")
					_s.append(AcceleroInfo + "\n");

				for (int i = 0; i < AcceleroValues.length; i++)
				{

					DecimalFormat df = new DecimalFormat("#.###");

					_s.append(df.format(AcceleroValues[i]));

					if (i < AcceleroValues.length - 1)
						_s.append("\n");
				}

			}
			catch (Exception ex)
			{
				_s.append("error while displaying values");
			}

			return _s.toString();
		}
	}

	/*
	 * Creates string for Gui
	 */
	public String lightString()
	{
		if (LightOn)
			return String.valueOf(LightValues);
		else
			return LightInfo;
	}

	/*
	 * Creates string for Gui
	 */
	public String touchString()
	{
		if (!TouchOn)
			return TouchInfo;
		else
		{
			_s.setLength(0);

			try
			{

				if (TouchTouchedValues == null || TouchXValues == null || TouchYValues == null)
				{
					_s.append("(no values)");
					return _s.toString();
				}

				for (int i = 0; i < 10; i++)
				{
					if (TouchTouchedValues[i])
					{
						DecimalFormat df = new DecimalFormat("#.###");
						_s.append(df.format(TouchXValues[i] / _xDim) + " , ");
						_s.append(df.format(TouchYValues[i] / _yDim));
					}
					else
					{
						_s.append("-");
					}

					if (i < 9)
						_s.append("\n");

				}
			}
			catch (Exception ex)
			{
				_s.append("error while displaying values");
			}

			return _s.toString();
		}
	}
}
