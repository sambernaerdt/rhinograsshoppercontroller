package be.bernaerdt.grasshoppercontroller.sending;

public interface ISendOut
{
	public void SendPackage(String infoToSend) throws Exception;
}
