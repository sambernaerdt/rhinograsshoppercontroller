package be.bernaerdt.grasshoppercontroller.sending;

import android.os.AsyncTask;
import android.util.Log;
import be.bernaerdt.grasshoppercontroller.ISendActivity;

public class SendTask extends AsyncTask<Void, Boolean, Void>
{
	private static final String TAG = SendTask.class.getSimpleName();
	
	private boolean _isActive;
	
	private ISendOut _sendOut;
	private ISendInfo _sendInfo;
	private int _sendSpeed;
	
	// Pointer to activity
	private ISendActivity _sendActivity;
	
	public SendTask(ISendOut sendOut, ISendInfo sendInfo, int sendSpeed, ISendActivity activity)
	{
		super();
		
		_sendInfo = sendInfo;
		_sendOut = sendOut;
		_sendSpeed = sendSpeed;
		_sendActivity = activity;
		_isActive = true;
	}
	
	@Override
	protected Void doInBackground(Void... params)
	{

		Log.i(TAG, "Sending - Started.");
		
		while(_isActive)
		{
			try
			{

				long startTime = System.nanoTime();
				
				String packString = _sendInfo.compose(";");
				_sendOut.SendPackage(packString);
				
				long waitFor = Math.max(0, _sendSpeed - (System.nanoTime() - startTime) / 1000000) ;

				publishProgress(true);

				Log.d(TAG, "Package sent.");
				Log.d(TAG, "Wait for: " + waitFor);
				
				Thread.sleep(waitFor);
				
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				publishProgress(false);
				
				Log.d(TAG, "Failed to send package.");
			}
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				publishProgress(false);
				
				Log.d(TAG, "Failed to send package.");
			}
			
		}

		Log.i(TAG, "Sending - Finished.");
		
		return null;
	}

	public void stopsending()
	{
		_isActive = false;
		Log.i(TAG, "Sending - Request to stop.");
	}
	
	protected void onProgressUpdate(Boolean... progress)
	{
		super.onProgressUpdate(progress);
		try
		{
			_sendActivity.SendProgressUpdated(progress[0]);
		}
		catch (Exception ex)
		{
			// Do nothing;
		}
	}

	protected void onPostExecute(Long result)
	{
		
	}

}
