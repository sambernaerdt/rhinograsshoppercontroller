package be.bernaerdt.grasshoppercontroller.wifi;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import android.net.DhcpInfo;
import android.net.wifi.WifiManager;

public class WifiUtilities
{
	public static InetAddress getBroadcastAddress(WifiManager wifiManager) throws IOException
	{
		DhcpInfo dhcp = wifiManager.getDhcpInfo();
		// handle null somehow

		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		return InetAddress.getByAddress(quads);
	}
	
	public static boolean isPortAvailable(int port)
	{
		try
		{
			ServerSocket srv = new ServerSocket(port);
			srv.close();
			srv = null;
			return true;

		}
		catch (IOException e)
		{
			return false;
		}
	}
}
