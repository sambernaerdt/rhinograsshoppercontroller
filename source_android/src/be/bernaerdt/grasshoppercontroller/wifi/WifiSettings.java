package be.bernaerdt.grasshoppercontroller.wifi;

import java.io.IOException;
import java.net.InetAddress;

import be.bernaerdt.grasshoppercontroller.InitTask;
import be.bernaerdt.grasshoppercontroller.gui.IReportProgressTo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

public class WifiSettings
{
	public static final int WIFI_ENABLE_TIMEOUT_MS = 5000;
	public static final int WIFI_CONNECT_TIMEOUT_MS = 10000;
	
	Activity _activity;
	
	public WifiManager wifiManager;
	public boolean startUpWifiEnabled;	// Store state at startup
	public String wifiName;

	// Broadcast Info
	public InetAddress inetAddress;
	public int port = 50025;
	
	// Target Info
	
	// For reporting
	InitTask _initTask;
	
	public WifiSettings(Activity activity)
	{
		_activity = activity;
	}

	public boolean tryToEnableWifi(InitTask task)
	{
		task.Report(null, "Enabling Wifi");
		wifiManager = (WifiManager) _activity.getSystemService(Context.WIFI_SERVICE);
		startUpWifiEnabled = true;
		if (!wifiManager.isWifiEnabled())
		{
			startUpWifiEnabled = false;
			wifiManager.setWifiEnabled(true);
			int i = 0;
			while (!wifiManager.isWifiEnabled() && i < 100)
			{
				try
				{
					Thread.sleep(WifiSettings.WIFI_ENABLE_TIMEOUT_MS/100);
				}
				catch (InterruptedException e)
				{
					//
				}
				i++;
			}
		}
		
		if (!wifiManager.isWifiEnabled())
		{
			task.Report("Enabling Wifi took too long.\n\nPlease turn on Wifi manually and restart.", null);
			return false;
		}
		else
		{
			task.Report("Wifi : Enabled", null);
			return true;
		}
	}

	public boolean tryToConnectWifi(InitTask task)
	{		
		task.Report(null, "Waiting for Wifi to connect");
		ConnectivityManager connManager = (ConnectivityManager) _activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		int i = 0;
		while (!mWifi.isConnected() && i < 100)
		{
			try
			{
				Thread.sleep(WifiSettings.WIFI_CONNECT_TIMEOUT_MS/100);
			}
			catch (InterruptedException e)
			{
				//
			}
			mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			i++;
		}
		if (mWifi.isConnected())
		{
			try
			{
				WifiInfo wifiInfo = wifiManager.getConnectionInfo();
				wifiName = wifiInfo.getSSID();
				task.Report("Wifi : Connected to " + wifiName, null);
			}
			catch (Exception ex)
			{
				task.Report("Wifi : Connected to unkown network", null);
			}
			return true;
		}
		else
		{
			task.Report("Wifi : Not Connected.\n\nConnected manually to a Wifi network and restart...",
					null);
			return false;
		}
	}


	public boolean tryToGetBroadcastAddress(InitTask task)
	{
		task.Report(null, "Getting Address");
		try
		{
			inetAddress = WifiUtilities.getBroadcastAddress(wifiManager);
			task.Report("InetAddress : " + inetAddress, null);
			return true;
		}
		catch (IOException e)
		{
			task.Report("Failed to get InetAddress : " + e.getMessage(), null);
			return false;
		}
	}
	
	public boolean tryToFindAvailablePort(InitTask task)
	{
		task.Report(null, "Searching available port");
		while (!WifiUtilities.isPortAvailable(port) && port <= 60000)
		{
			port = port + 1;
		}
		if (port >= 60000)
		{
			task.Report("\nCould not find an available port! Enable at least one port between 50025 and 60000.", null);
			return false;
		}
		else
		{
			task.Report("Port : " + port, null);
			return true;
		}
	}
	
	public void putToIntent(Intent i)
	{
		i.putExtra("iNetAddress", inetAddress);
		i.putExtra("port", port);
	}
}
