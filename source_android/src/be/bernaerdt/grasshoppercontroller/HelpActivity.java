package be.bernaerdt.grasshoppercontroller;

import android.app.Activity;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.view.Window;

public class HelpActivity extends Activity
{
	private int _curPage = 1;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// TODO: disable rotation -> always portrait

		// ----------
		// GUI
		// ----------
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.help1);
	}

	public void previous(View view)
	{
		_curPage--;
		
		updateContent();
	}

	public void next(View view)
	{
		_curPage++;

		updateContent();
	}

	private void updateContent()
	{
		if (_curPage > 5 || _curPage < 1)
			finish();
		
		switch (_curPage)
		{
		case 1:
			setContentView(R.layout.help1);
			break;
		case 2:
			setContentView(R.layout.help2);
			break;
		case 3:
			setContentView(R.layout.help3);
			break;
		case 4:
			setContentView(R.layout.help4);
			break;
		case 5:
			setContentView(R.layout.help5);
			break;
		}
	}
}
