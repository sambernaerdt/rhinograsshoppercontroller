package be.bernaerdt.grasshoppercontroller.Settings;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.SensorManager;

public class MainSettings extends SettingsBase
{
	// Settings
	public SettingInt
			sensorSpeed,
			sendSpeed;
	public SettingBoolean
			accelerometerOn,
			orientationOn,
			lightOn,
			touchOn;
	
	public MainSettings(SharedPreferences prefs)
	{
		super(prefs);
	}

	@Override
	protected void _initialize()
	{
		// Store all settings in a list for easier operating on them all
		
		Map<Integer, Integer> speedMap = new Hashtable<Integer, Integer>();
		speedMap.put(0, 1000);
		speedMap.put(1, 500);
		speedMap.put(2, 250);
		speedMap.put(3, 100);
		speedMap.put(4, 50);
		speedMap.put(5, 20);		
		_settings.add(sensorSpeed = new SettingInt("SensorSpeed", 0, speedMap));
		
		speedMap.clear();
		speedMap.put(0, SensorManager.SENSOR_DELAY_NORMAL);
		speedMap.put(1, SensorManager.SENSOR_DELAY_GAME);		
		_settings.add(sendSpeed = new SettingInt("SendSpeed", 0, speedMap));
		
		_settings.add(accelerometerOn = new SettingBoolean("Accelorometer", true));
		
		_settings.add(orientationOn = new SettingBoolean("Orientation", true));
		
		_settings.add(lightOn = new SettingBoolean("Light", true));
		
		_settings.add(touchOn = new SettingBoolean("Touch", true));
	}

}
