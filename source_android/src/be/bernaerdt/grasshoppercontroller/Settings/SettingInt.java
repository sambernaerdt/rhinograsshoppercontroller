package be.bernaerdt.grasshoppercontroller.Settings;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.R.integer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class SettingInt extends SettingBase
{
	private static final String TAG = SettingInt.class.getName();
	private int _value;
	private int _defaultValue;
	private Map<Integer,Integer> _spinnerValues;
	
	public SettingInt(String name, int defaultValue, Map<Integer, Integer> spinnerValues)
	{
		super(name);
		
		_defaultValue = _value = defaultValue;
		_spinnerValues = spinnerValues;
	}

	// TODO: can merge to abstract / interface
	public int getValue()
	{
		return _value;
	}
	
	@Override
	public void readFromPrefs(SharedPreferences prefs)
	{
		_value = prefs.getInt(getName(), _defaultValue);
	}

	@Override
	public void saveToPrefs(Editor edit)
	{
		edit.putInt(getName(), _value);
	}

	@Override
	public void setFromSpinnerPosition(int pos) throws Exception
	{
		if(_spinnerValues.containsKey(pos))
			_value = _spinnerValues.get(pos);
		else
			throw new Exception("Invalid Spinner Position");
	}

	@Override
	public void saveToIntent(Intent intent)
	{
		intent.putExtra(getName(), getValue());
	}

	@Override
	public int getSpinnerPosition()
	{
		try
		{
			if(!_spinnerValues.containsValue(_value))
				throw new Exception("Value " + _value + " is not present in spinner map.");
			
			// Find first occurrence in HashTable
			for(Entry<Integer, Integer> entry : _spinnerValues.entrySet())
				if(entry.getValue()==_value)
					return entry.getKey();
			
			throw new Exception("Error while retrieving key of " + _value);
					
		}
		catch (Exception e)
		{
			Log.d(TAG, e.getMessage());
			return 0;
		}
	}
}
