package be.bernaerdt.grasshoppercontroller.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SettingBoolean extends SettingBase
{
	private boolean _value;
	private boolean _defaultValue;

	public SettingBoolean(String name, boolean defaultValue)
	{
		super(name);

		_defaultValue = _value = defaultValue;
	}

	public boolean getValue()
	{
		return _value;
	}

	@Override
	public void readFromPrefs(SharedPreferences prefs)
	{
		_value = prefs.getBoolean(getName(), _defaultValue);
	}

	@Override
	public void saveToPrefs(Editor edit)
	{
		edit.putBoolean(getName(), _value);
	}

	@Override
	public void setFromSpinnerPosition(int pos) throws Exception
	{
		// For all booleans in this program:
		// 0 = ON (true)
		// 1 = OFF (false)

		if (pos == 0)
			_value = true;
		else if (pos == 1)
			_value = false;
		else
			throw new Exception("Invalid spinner position");
	}

	@Override
	public void saveToIntent(Intent intent)
	{
		intent.putExtra(getName(), getValue());
	}

	@Override
	public int getSpinnerPosition()
	{
		if(_value)
			return 0;
		else
			return 1;
	}
}
