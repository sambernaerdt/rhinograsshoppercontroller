package be.bernaerdt.grasshoppercontroller.Settings;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public abstract class SettingsBase
{
	protected List<SettingBase> _settings;
	
	protected SharedPreferences _prefs;
	
	public SettingsBase(SharedPreferences prefs)
	{
		_settings = new ArrayList<SettingBase>();
		_initialize();
		
		_prefs = prefs;
		_readSettings();
	}
	
	protected void _readSettings()
	{
		for(int i=0; i<_settings.size(); i++)
			_settings.get(i).readFromPrefs(_prefs);
	}

	public void Save()
	{
		Editor edit = _prefs.edit();

		for(int i=0; i<_settings.size(); i++)
			_settings.get(i).saveToPrefs(edit);
		
		edit.commit();
	}
	
	public void putToIntent(Intent intent)
	{
		for(int i=0; i<_settings.size(); i++)
			_settings.get(i).saveToIntent(intent);
	}
	
	protected abstract void _initialize();
	
}
