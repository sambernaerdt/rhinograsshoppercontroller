package be.bernaerdt.grasshoppercontroller.Settings;

import java.util.List;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public abstract class SettingBase 
{
	private String name; 
	
	public SettingBase(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public abstract void saveToIntent(Intent intent);
	public abstract void readFromPrefs(SharedPreferences prefs);
	public abstract void saveToPrefs(Editor edit);
	public abstract void setFromSpinnerPosition(int pos) throws Exception;
	public abstract int getSpinnerPosition();
	
}
