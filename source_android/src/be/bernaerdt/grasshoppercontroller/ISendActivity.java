package be.bernaerdt.grasshoppercontroller;

public interface ISendActivity
{
	public void SensorChanged();
	
	public void SendProgressUpdated(boolean success);
}
