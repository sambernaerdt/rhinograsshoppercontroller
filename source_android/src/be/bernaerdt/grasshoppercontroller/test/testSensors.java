package be.bernaerdt.grasshoppercontroller.test;

import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;
import be.bernaerdt.grasshoppercontroller.ISendActivity;
import be.bernaerdt.grasshoppercontroller.R;
import be.bernaerdt.grasshoppercontroller.gui.SendGUI;
import be.bernaerdt.grasshoppercontroller.registering.IRegisterSensors;
import be.bernaerdt.grasshoppercontroller.registering.Listeners;
import be.bernaerdt.grasshoppercontroller.registering.RegisterSensors;
import be.bernaerdt.grasshoppercontroller.sending.ISendOut;
import be.bernaerdt.grasshoppercontroller.sending.SendInfo;

public class testSensors extends Activity implements ISendActivity
{

	SendGUI _sendGUI;
	WakeLock _wakeLock;
	ISendOut _sendOut;
	
	// sendInfo
	private SendInfo _sendInfo;
	
	public SensorManager _sensorManager;
	private Listeners _listeners;
	private IRegisterSensors _registerSensors;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// -----------
		// GUI
		// -----------

		// Initializing
		setContentView(R.layout.sending);
		_sendGUI = new SendGUI(this);

		// WakeLock
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		_wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "ControllerLock");
		
		// Fake sender
		_sendOut = new SendOutStub();
		
		_sendGUI.hideError();
		
		_sendInfo = new SendInfo(480, 800);
		
		registerAllSensors();
		_sendGUI.updateGUIFields(_sendInfo);
	}

	public void registerAllSensors()
	{
		// Create Listener, manager
		if (_listeners == null)
			_listeners = new Listeners(_sendInfo, this);
		if (_sensorManager == null)
			_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		_registerSensors = new RegisterSensors(_sendGUI.getRoot(), _sendInfo, _listeners, _sensorManager);
		_registerSensors.Register(true, true, true, true, SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void SensorChanged()
	{
		try
		{
			_sendGUI.updateGUIFields(_sendInfo);
			_sendGUI.updateSucces();
		}
		catch (Exception e)
		{
			_sendGUI.updateFail();
		}
	}

	public void SendProgressUpdated(boolean success)
	{
		// Do nothing
	}

}
