package be.bernaerdt.grasshoppercontroller.test;

import be.bernaerdt.grasshoppercontroller.sending.ISendOut;

public class SendOutStub implements ISendOut
{

	public void SendPackage(String infoToSend) throws Exception
	{
		// Don't do anything
	}

}
