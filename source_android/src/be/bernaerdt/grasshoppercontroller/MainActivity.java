package be.bernaerdt.grasshoppercontroller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import be.bernaerdt.grasshoppercontroller.Settings.MainSettings;
import be.bernaerdt.grasshoppercontroller.gui.MainGUI;
import be.bernaerdt.grasshoppercontroller.wifi.WifiSettings;

public class MainActivity extends Activity
{
	private static final String TAG = MainActivity.class.getName();
	public MainGUI mainGUI;
	public MainSettings mainSettings;
	public WifiSettings wifiSettings;
	
	private InitTask _initTask;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		_setUpSettings();
		_setUpGUI();
		wifiSettings = new WifiSettings(this);

		_initTask = new InitTask(this);
		_initTask.execute(this);
	}

	private void _setUpGUI()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		mainGUI = new MainGUI(this);
	}

	private void _setUpSettings()
	{
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		mainSettings = new MainSettings(prefs);
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		try
		{
			mainGUI.updateSettings(mainSettings);
			mainSettings.Save();
		}
		catch (Exception e)
		{
			Log.e(TAG, "Unable to store settings");
			Log.d(TAG, e.getMessage());
		}
	}

	/*
	 * HANDLE USER INPUT
	 */

	public void startSending(View view) throws Exception
	{
		// TODO OCP violation
		Intent i = new Intent(this, SendActivity.class);

		i.putExtra("metricsWidth", getResources().getDisplayMetrics().widthPixels);
		i.putExtra("metricsHeight", getResources().getDisplayMetrics().heightPixels);

		mainGUI.updateSettings(mainSettings);
		mainSettings.putToIntent(i);

		wifiSettings.putToIntent(i);
        
		startActivity(i);
	}

	public void showHelp(View view)
	{
		Intent i = new Intent(this, HelpActivity.class);
		startActivity(i);
	}

}
