package be.bernaerdt.grasshoppercontroller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import be.bernaerdt.grasshoppercontroller.gui.SendGUI;
import be.bernaerdt.grasshoppercontroller.registering.IRegisterSensors;
import be.bernaerdt.grasshoppercontroller.registering.Listeners;
import be.bernaerdt.grasshoppercontroller.registering.RegisterSensors;
import be.bernaerdt.grasshoppercontroller.sending.ISendOut;
import be.bernaerdt.grasshoppercontroller.sending.SendInfo;
import be.bernaerdt.grasshoppercontroller.sending.SendOutIP;
import be.bernaerdt.grasshoppercontroller.sending.SendTask;

public class SendActivity extends Activity implements ISendActivity
{
	private static final String TAG = SendActivity.class.getName();
	
	// activity
	private boolean _inForeground = false;
	public WakeLock _wakeLock;

	// Sensors
	public SensorManager _sensorManager;
	private Listeners _listeners;
	private IRegisterSensors _registerSensors;

	// Settings
	private boolean _orientationOn;
	private boolean _acceleroOn;
	private boolean _lightOn;
	private boolean _touchOn;
	private int _updateSpeed;
	private int _sendSpeed;

	// Sending
	private SendTask _sendTask ;
	private ISendOut _sendOut ;
	
	// GUI
	private SendGUI _sendGUI;
	
	// sendInfo
	private SendInfo _sendInfo;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// -----------
		// GUI
		
		// -----------

		// Initializing
		setContentView(R.layout.sending);
		_sendGUI = new SendGUI(this);

		// WakeLock
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		_wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "ControllerLock");

		// -----------
		// STARTUP ARGS
		// -----------
		
		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			_sendGUI.setError("Error receiving settings.");
			return;
		}

		if(!_setupNetwork(extras))
			return;
		
		if(!_setupSendInfo(extras))
			return;
		
		_setupSettings(extras);
		
		// -----------
		// SENSORS
		// -----------

		_sendGUI.hideError();

	}

	private boolean _setupNetwork(Bundle extras)
	{
		// Network info
		InetAddress inetAddress = (InetAddress) extras.get("iNetAddress");
		int port = extras.getInt("port");
		_sendSpeed = extras.getInt("sendSpeed");
		
		if (inetAddress == null || port == 0 || _sendSpeed == 0)
		{
			_sendGUI.setError("Internal Error : unable to read settings to set up Sending.");
			return false;
		}
		
		//_sendOut = new SendOutBroudcast(inetAddress, port);
		try
		{
			_sendOut = new SendOutIP(InetAddress.getByName("192.168.0.163"), port);
		}
		catch (UnknownHostException e)
		{
			// TODO Auto-generated catch block
			Log.e(TAG, e.getMessage());
			return false;
		}
		return true;
	}

	private void _setupSettings(Bundle extras)
	{
		// Settings
		_acceleroOn = extras.getBoolean("acceleroOn");
		_lightOn = extras.getBoolean("lightOn");
		_orientationOn = extras.getBoolean("rotationOn");
		_touchOn = extras.getBoolean("touchOn");
		_updateSpeed = extras.getInt("speed");
	}

	private boolean _setupSendInfo(Bundle extras)
	{
		// SendInfo
		int sendInfoX = extras.getInt("metricsWidth");
		int sendInfoY = extras.getInt("metricsHeight");
		_sendInfo = new SendInfo(sendInfoX, sendInfoY);
		
		if ( _sendInfo == null)
		{
			_sendGUI.setError("Error reading settings.");
			return false;
		}
		
		return true;
	}

	/*
	 * Registers all sensors
	 */
	
	private void registerAllSensors()
	{
		// Create Listener, manager
		if (_listeners == null)
			_listeners = new Listeners(_sendInfo, this);
		if (_sensorManager == null)
			_sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		_registerSensors = new RegisterSensors(_sendGUI.getRoot(), _sendInfo, _listeners, _sensorManager);
		_registerSensors.Register(_touchOn, _lightOn, _acceleroOn, _orientationOn, _updateSpeed);
	}

	public void SensorChanged()
	{
		_sendGUI.updateGUIFields(_sendInfo);
	}
	
	/*
	 * APPLICATION LIFETIME
	 */
	
	@Override
	public void onResume()
	{
		_inForeground = true;
		if (_wakeLock != null)
			_wakeLock.acquire();

		registerAllSensors();
		_sendGUI.updateGUIFields(_sendInfo);
		
		_sendTask = new SendTask(_sendOut, _sendInfo, _sendSpeed, this);
		_sendTask.execute(null);

		super.onResume();
	}

	@Override
	public void onPause()
	{
		_inForeground = false;
		if (_wakeLock != null)
			_wakeLock.release();

		_sensorManager.unregisterListener(_listeners);

		_sendTask.stopsending();

		super.onPause();
	}

	public void SendProgressUpdated(boolean success)
	{
		if(success)
			_sendGUI.updateSucces();
		else
			_sendGUI.updateFail();
	}


}
