package be.bernaerdt.grasshoppercontroller;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ShowSettingsSensorActivity extends PreferenceActivity
{
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{    
	
	    super.onCreate(savedInstanceState);       
	    addPreferencesFromResource(R.xml.preferences_sensors);       
	
	}

}
