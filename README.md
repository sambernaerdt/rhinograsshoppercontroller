Rhino Grasshopper Controller Android
==
Android app sending sensor data over WIFI broadcast. Can be received with GHowl for Grasshopper for Rhino to be used as inputdata.

More info at the [Play Store]

Please send me an [email] if you wish to contribute to this project.

Notes:

* Project wasn't created with public repository in mind and is poorly documented
* Java conventions are not respected
* Project was created early 2012 and not updated since

[Play Store]:https://play.google.com/store/apps/details?id=be.bernaerdt.grasshoppercontroller
[email]:sam@bernaerdt.be    